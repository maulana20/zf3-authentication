# zf3-authentication

## Getting Started

Bersumber dari https://github.com/olegkrivtsov/using-zf3-book-samples/tree/master/userdemo .

Kemudian ambil library zend.
```bash
composer install
```

Kemudian migrate table.
```bash
vendor\bin\doctrine-module migrations:migrate
```

![inspect](https://gitlab.com/maulana20/zf3-authentication/-/raw/master/screen/migrate.PNG)

Running local :
```bash
php -S 0.0.0.0:8080 -t public public/index.php
```

![login](https://gitlab.com/maulana20/zf3-authentication/-/raw/master/screen/login.PNG)

![login](https://gitlab.com/maulana20/zf3-authentication/-/raw/master/screen/home.PNG)

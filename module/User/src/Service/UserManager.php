<?php
namespace User\Service;

use User\Entity\User;
use Zend\Crypt\Password\Bcrypt;
use Zend\Math\Rand;
use Zend\Mail;
use Zend\Mail\Transport\Smtp as SmtpTransport;
use Zend\Mail\Transport\SmtpOptions;
use Zend\Mime\Message as MimeMessage;
use Zend\Mime\Part as MimePart;

class UserManager
{
	private $entitymanager;
	private $viewrenderer;
	private $config;
	
	public function __construct($entitymanager, $viewrenderer, $config) 
	{
		$this->entitymanager = $entitymanager;
		$this->viewrenderer = $viewrenderer;
		$this->config = $config;
	}
	
	public function addUser($data) 
	{
		if ($this->checkUserExists($data['email'])) throw new \Exception("User with email address " . $data['$email'] . " already exists");
		
		$user = new User();
		$bcrypt = new Bcrypt();
		
		$user->setEmail($data['email']);
		$user->setFullName($data['full_name']);        
		$user->setPassword($bcrypt->create($data['password']));
		$user->setStatus($data['status']);
		$user->setDateCreated(date('Y-m-d H:i:s'));
		
		$this->entitymanager->persist($user); // Add the entity to the entity manager.
		
		$this->entitymanager->flush(); // Apply changes to database.
		
		return $user;
	}

	public function updateUser($user, $data) 
	{
		if($user->getEmail() != $data['email'] && $this->checkUserExists($data['email'])) throw new \Exception("Another user with email address " . $data['email'] . " already exists");
		
		$user->setEmail($data['email']);
		$user->setFullName($data['full_name']);        
		$user->setStatus($data['status']);        
		
		$this->entitymanager->flush(); // Apply changes to database.
		
		return true;
	}
	
	public function createAdminUserIfNotExists()
	{
		$user = $this->entitymanager->getRepository(User::class)->findOneBy([]);
		
		if ($user == null) {
			$user = new User();
			$user->setEmail('admin@example.com');
			$user->setFullName('Admin Test');
			
			$bcrypt = new Bcrypt();
			
			$user->setPassword($bcrypt->create('password'));
			$user->setStatus(User::STATUS_ACTIVE);
			$user->setDateCreated(date('Y-m-d H:i:s'));
			
			$this->entitymanager->persist($user);
			
			$this->entitymanager->flush();
		}
	}
	
	public function checkUserExists($email)
	{
		$user = $this->entitymanager->getRepository(User::class)->findOneByEmail($email);
		
		return $user !== null;
	}
	
	public function validatePassword($user, $password) 
	{
		$bcrypt = new Bcrypt();
		
		if ($bcrypt->verify($password, $user->getPassword())) return true;
		
		return false;
	}
	
	public function generatePasswordResetToken($user)
	{
		if ($user->getStatus() != User::STATUS_ACTIVE) throw new \Exception('Cannot generate password reset token for inactive user ' . $user->getEmail());
		
		$token = Rand::getString(32, '0123456789abcdefghijklmnopqrstuvwxyz', true);
		
		$bcrypt = new Bcrypt();
		
		$user->setPasswordResetToken($bcrypt->create($token));
		$user->setPasswordResetTokenCreationDate(date('Y-m-d H:i:s'));  
		
		$this->entitymanager->flush(); // Apply changes to DB.
		
		$password_reset_url = null;
		$password_reset_url = 'http://' . (isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : 'localhost') . '/set-password?token=' . $token . "&email=" . $user->getEmail();
		
		$bodyhtml = $this->viewrenderer->render('user/email/reset-password-email', [ 'passwordResetUrl' => $password_reset_url, ]);
		
		$html = new MimePart($bodyhtml);
		$html->type = "text/html";
		
		$body = new MimeMessage();
		$body->addPart($html);
		
		$mail = new Mail\Message();
		$mail->setEncoding('UTF-8');
		$mail->setBody($body);
		$mail->setFrom('no-reply@example.com', 'User Demo');
		$mail->addTo($user->getEmail(), $user->getFullName());
		$mail->setSubject('Password Reset');
		
		// Setup SMTP transport
		$transport = new SmtpTransport();
		$options   = new SmtpOptions($this->config['smtp']);
		$transport->setOptions($options);
		
		$transport->send($mail);
	}
	
	public function validatePasswordResetToken($email, $password_reset_token)
	{
		$user = $this->entitymanager->getRepository(User::class)->findOneByEmail($email);
		
		if($user == null || $user->getStatus() != User::STATUS_ACTIVE) return false;
		
		$bcrypt = new Bcrypt();
		
		if (!$bcrypt->verify($password_reset_token, $user->getPasswordResetToken())) return false;
		
		if (strtotime('now') - strtotime($user->getPasswordResetTokenCreationDate()) > 24 * 60 * 60) return false;
		
		return true;
	}
	
	public function setNewPasswordByToken($email, $password_reset_token, $new_password)
	{
		if (!$this->validatePasswordResetToken($email, $password_reset_token)) return false;
		
		$user = $this->entitymanager->getRepository(User::class)->findOneByEmail($email);
		
		if ($user == null || $user->getStatus() != User::STATUS_ACTIVE) return false;
		
		$bcrypt = new Bcrypt();
		  
		$user->setPassword($bcrypt->create($new_password));
		$user->setPasswordResetToken(null);
		$user->setPasswordResetTokenCreationDate(null);
		
		$this->entitymanager->flush();
		
		return true;
	}
	
	public function changePassword($user, $data)
	{
		if (!$this->validatePassword($user, $data['old_password'])) return false;
		
		if (strlen($data['new_password']) < 6 || strlen($data['new_password']) > 64) return false;
		
		$bcrypt = new Bcrypt();
		
		$user->setPassword($bcrypt->create($data['new_password']));
		
		$this->entitymanager->flush();
		
		return true;
	}
}


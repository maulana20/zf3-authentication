<?php
namespace User\Service;

use Zend\Authentication\Result;

class AuthManager
{
	private $authservice;
	private $sessionmanager;
	private $config;
	
	public function __construct($authservice, $sessionmanager, $config) 
	{
		$this->authservice = $authservice;
		$this->sessionmanager = $sessionmanager;
		$this->config = $config;
	}
	
	public function login($email, $password, $rememberme)
	{   
		if ($this->authservice->getIdentity() != null) throw new \Exception('Already logged in');
		
		$authadapter = $this->authservice->getAdapter();
		$authadapter->setEmail($email);
		$authadapter->setPassword($password);
		
		$result = $this->authservice->authenticate();
		
		if ($result->getCode() == Result::SUCCESS && $rememberme) $this->sessionmanager->rememberMe(60 * 60 * 24 * 30);
		
		return $result;
	}
	
	public function logout()
	{
		if ($this->authservice->getIdentity() == null) throw new \Exception('The user is not logged in');
		
		$this->authservice->clearIdentity();               
	}
	
	public function filterAccess($controller, $action)
	{
		$mode = isset($this->config['options']['mode']) ? $this->config['options']['mode'] : 'restrictive';
		
		if ($mode != 'restrictive' && $mode != 'permissive') throw new \Exception('Invalid access filter mode (expected either restrictive or permissive mode');
		
		if (isset($this->config['controllers'][$controller])) {
			foreach ($this->config['controllers'][$controller] as $item) {
				if (is_array($item['actions']) && in_array($action, $item['actions']) || $item['actions'] == '*') {
					if ($item['allow'] == '*') return true; // Anyone is allowed to see the page.
					if ($item['allow'] == '@' && $this->authservice->hasIdentity()) return true; // Only authenticated user is allowed to see the page.
					
					return false; // Access denied.
				}
			}
		}
		
		if ($mode == 'restrictive' && !$this->authservice->hasIdentity()) return false;
		
		return true;
	}
}

<?php
namespace User\Service\Factory;

use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;
use Zend\Authentication\AuthenticationService;
use Zend\Session\SessionManager;
use User\Service\AuthManager;

class AuthManagerFactory implements FactoryInterface
{
	public function __invoke(ContainerInterface $container, $requested_name, array $options = null)
	{
		$config = $container->get('Config');
		$config = (isset($config['access_filter'])) ? $config['access_filter'] : [];
		
		return new AuthManager($container->get(\Zend\Authentication\AuthenticationService::class), $container->get(SessionManager::class), $config);
	}
}

<?php
namespace User\Validator;

use Zend\Validator\AbstractValidator;
use User\Entity\User;

class UserExistsValidator extends AbstractValidator 
{
	protected $options = [ 'entityManager' => null, 'user' => null ];
	
	const NOT_SCALAR  = 'notScalar';
	const USER_EXISTS = 'userExists';
	
	protected $messageTemplates = [ self::NOT_SCALAR  => "The email must be a scalar value", self::USER_EXISTS  => "Another user with such an email already exists" ];
	
	public function __construct($options = null) 
	{
		if (is_array($options)) {
			if(isset($options['entityManager'])) $this->options['entityManager'] = $options['entityManager'];
			if(isset($options['user'])) $this->options['user'] = $options['user'];
		}
		
		parent::__construct($options);
	}
	
	public function isValid($value) 
	{
		if (!is_scalar($value)) {
			$this->error(self::NOT_SCALAR);
			return false; 
		}
		
		$entitymanager = $this->options['entityManager'];
		
		$user = $entitymanager->getRepository(User::class)->findOneByEmail($value);
		
		if ($this->options['user'] == null) {
			$is_valid = ($user == null);
		} else {
			$is_valid = ($this->options['user']->getEmail() != $value && $user != null) ? false : true;
		}
		
		if (!$is_valid) $this->error(self::USER_EXISTS);
		
		return $is_valid;
	}
}


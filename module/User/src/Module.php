<?php
namespace User;

use Zend\Mvc\MvcEvent;
use Zend\Mvc\Controller\AbstractActionController;
use User\Controller\AuthController;
use User\Service\AuthManager;

class Module
{
	public function getConfig()
	{
		return include __DIR__ . '/../config/module.config.php';
	}
	
	public function onBootstrap(MvcEvent $event)
	{
		$eventmanager = $event->getApplication()->getEventManager();
		
		$sharedeventmanager = $eventmanager->getSharedManager();
		$sharedeventmanager->attach(AbstractActionController::class, MvcEvent::EVENT_DISPATCH, [$this, 'onDispatch'], 100);
		
		$sessionmanager = $event->getApplication()->getServiceManager()->get('Zend\Session\SessionManager');
		
		$this->forgetInvalidSession($sessionmanager);
	}
	
	protected function forgetInvalidSession($sessionmanager) 
	{
		try {
			$sessionmanager->start();
			return;
		} catch (\Exception $e) {
		}
		
		session_unset();
	}
	
	public function onDispatch(MvcEvent $event)
	{
		$controller = $event->getTarget();
		
		$controller_name = $event->getRouteMatch()->getParam('controller', null);
		
		$action_name = $event->getRouteMatch()->getParam('action', null);
		$action_name = str_replace('-', '', lcfirst(ucwords($action_name, '-')));
		
		$authmanager = $event->getApplication()->getServiceManager()->get(AuthManager::class);
		
		if ($controller_name != AuthController::class && !$authmanager->filterAccess($controller_name, $action_name)) {
			$uri = $event->getApplication()->getRequest()->getUri();
			$uri->setScheme(null)->setHost(null)->setPort(null)->setUserInfo(null);
			
			return $controller->redirect()->toRoute('login', [], [ 'query' => [ 'redirectUrl' => $uri->toString() ] ] );
		}
	}
}

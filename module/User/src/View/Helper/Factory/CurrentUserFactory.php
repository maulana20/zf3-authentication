<?php
namespace User\View\Helper\Factory;

use Interop\Container\ContainerInterface;
use User\View\Helper\CurrentUser;

class CurrentUserFactory
{
	public function __invoke(ContainerInterface $container)
	{
		return new CurrentUser($container->get('doctrine.entitymanager.orm_default'), $container->get(\Zend\Authentication\AuthenticationService::class));
	}
}

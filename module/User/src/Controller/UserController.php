<?php
namespace User\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use DoctrineORMModule\Paginator\Adapter\DoctrinePaginator as DoctrineAdapter;
use Doctrine\ORM\Tools\Pagination\Paginator as ORMPaginator;
use Zend\Paginator\Paginator;
use Application\Entity\Post;
use User\Entity\User;
use User\Form\UserForm;
use User\Form\PasswordChangeForm;
use User\Form\PasswordResetForm;

class UserController extends AbstractActionController 
{
	private $entitymanager;
	private $usermanager;
	
	public function __construct($entitymanager, $usermanager)
	{
		$this->entitymanager = $entitymanager;
		$this->usermanager = $usermanager;
	}
	
	public function indexAction() 
	{
		$page = $this->params()->fromQuery('page', 1);
		
		$query = $this->entitymanager->getRepository(User::class)->findAllUsers();
		
		$adapter = new DoctrineAdapter(new ORMPaginator($query, false));
		$paginator = new Paginator($adapter);
		
		$paginator->setDefaultItemCountPerPage(10);        
		$paginator->setCurrentPageNumber($page);
		
		return new ViewModel([ 'users' => $paginator ]);
	}
	
	public function addAction()
	{
		$form = new UserForm('create', $this->entitymanager);
		
		if ($this->getRequest()->isPost()) {
			$data = $this->params()->fromPost();            
			$form->setData($data);
			
			if($form->isValid()) {
				$data = $form->getData();
				$user = $this->usermanager->addUser($data);
				
				return $this->redirect()->toRoute('users', [ 'action' => 'view', 'id' => $user->getId() ]);                
			}
		} 
		
		return new ViewModel([ 'form' => $form ]);
	}
	
	public function viewAction()
	{
		$id = (int) $this->params()->fromRoute('id', -1);
		
		if ($id < 1) {
			$this->getResponse()->setStatusCode(404);
			return;
		}
		
		$user = $this->entitymanager->getRepository(User::class)->find($id);
		
		if ($user == null) {
			$this->getResponse()->setStatusCode(404);
			return;
		}
		
		return new ViewModel([ 'user' => $user ]);
	}
	
	public function editAction() 
	{
		$id = (int) $this->params()->fromRoute('id', -1);
		
		if ($id < 1) {
			$this->getResponse()->setStatusCode(404);
			return;
		}
		
		$user = $this->entitymanager->getRepository(User::class)->find($id);
		
		if ($user == null) {
			$this->getResponse()->setStatusCode(404);
			return;
		}
		
		$form = new UserForm('update', $this->entitymanager, $user);
		
		if ($this->getRequest()->isPost()) {
			$data = $this->params()->fromPost();
			$form->setData($data);
			
			if($form->isValid()) {
				$data = $form->getData();
				$this->usermanager->updateUser($user, $data);
				
				return $this->redirect()->toRoute('users', [ 'action' => 'view', 'id' => $user->getId() ]);
			}
		} else {
			$form->setData([ 'full_name' => $user->getFullName(), 'email' => $user->getEmail(), 'status' => $user->getStatus() ]);
		}
		
		return new ViewModel([ 'user' => $user, 'form' => $form ]);
	}
	
	public function changePasswordAction() 
	{
		$id = (int) $this->params()->fromRoute('id', -1);
		
		if ($id < 1) {
			$this->getResponse()->setStatusCode(404);
			return;
		}
		
		$user = $this->entitymanager->getRepository(User::class)->find($id);
		
		if ($user == null) {
			$this->getResponse()->setStatusCode(404);
			return;
		}
		
		$form = new PasswordChangeForm('change');
		
		if ($this->getRequest()->isPost()) {
			$data = $this->params()->fromPost();            
			$form->setData($data);
			
			if($form->isValid()) {
				$data = $form->getData();
				
				if (!$this->usermanager->changePassword($user, $data)) {
					$this->flashMessenger()->addErrorMessage('Sorry, the old password is incorrect. Could not set the new password.');
				} else {
					$this->flashMessenger()->addSuccessMessage('Changed the password successfully.');
				}
				
				return $this->redirect()->toRoute('users', ['action' => 'view', 'id' => $user->getId() ]);                
			}
		}
		
		return new ViewModel([ 'user' => $user, 'form' => $form ]);
	}
	
	public function resetPasswordAction()
	{
		$form = new PasswordResetForm();
		
		if ($this->getRequest()->isPost()) {
			$data = $this->params()->fromPost();            
			$form->setData($data);
			
			if($form->isValid()) {
				$user = $this->entitymanager->getRepository(User::class)->findOneByEmail($data['email']);
				
				if ($user != null && $user->getStatus() == User::STATUS_ACTIVE) {
					$this->usermanager->generatePasswordResetToken($user);
					
					return $this->redirect()->toRoute('users', [ 'action' => 'message', 'id' => 'sent' ]);
				} else {
					return $this->redirect()->toRoute('users', [ 'action' => 'message', 'id' => 'invalid-email' ]);
				}
			}
		}
		
		return new ViewModel([ 'form' => $form ]);
	}
	
	public function messageAction() 
	{
		$id = (string) $this->params()->fromRoute('id');
		
		if ($id != 'invalid-email' && $id != 'sent' && $id != 'set' && $id != 'failed') throw new \Exception('Invalid message ID specified');
		
		return new ViewModel([ 'id' => $id ]);
	}
	
	public function setPasswordAction()
	{
		$email = $this->params()->fromQuery('email', null);
		$token = $this->params()->fromQuery('token', null);
		
		if ($token != null && (!is_string($token) || strlen($token) != 32)) throw new \Exception('Invalid token type or length');
		if ($token === null || !$this->usermanager->validatePasswordResetToken($email, $token)) return $this->redirect()->toRoute('users', [ 'action' => 'message', 'id' => 'failed' ]);
		
		$form = new PasswordChangeForm('reset');
		
		if ($this->getRequest()->isPost()) {
			$data = $this->params()->fromPost();
			$form->setData($data);
			
			if($form->isValid()) {
				$data = $form->getData();
				
				if ($this->usermanager->setNewPasswordByToken($email, $token, $data['new_password'])) {
					return $this->redirect()->toRoute('users', [ 'action' => 'message', 'id' => 'set' ]);
				} else {
					return $this->redirect()->toRoute('users', [ 'action' => 'message', 'id' => 'failed' ]);                 
				}
			}
		}
		
		return new ViewModel([ 'form' => $form ]);
	}
}

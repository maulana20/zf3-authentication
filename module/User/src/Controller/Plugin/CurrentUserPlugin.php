<?php
namespace User\Controller\Plugin;

use Zend\Mvc\Controller\Plugin\AbstractPlugin;
use User\Entity\User;

class CurrentUserPlugin extends AbstractPlugin
{
	private $entitymanager;
	private $authservice;
	private $user = null;
	
	public function __construct($entitymanager, $authservice) 
	{
		$this->entitymanager = $entitymanager;
		$this->authservice = $authservice;
	}
	
	public function __invoke($use_cached_user = true)
	{
		if ($use_cached_user && $this->user !== null) return $this->user;
		
		if ($this->authservice->hasIdentity()) {
			$this->user = $this->entitymanager->getRepository(User::class)->findOneByEmail($this->authservice->getIdentity());
			
			if ($this->user == null) throw new \Exception('Not found user with such email');
			
			return $this->user;
		}
		
		return null;
	}
}

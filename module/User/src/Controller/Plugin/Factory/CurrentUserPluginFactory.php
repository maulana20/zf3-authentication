<?php
namespace User\Controller\Plugin\Factory;

use Interop\Container\ContainerInterface;
use User\Controller\Plugin\CurrentUserPlugin;

class CurrentUserPluginFactory
{
	public function __invoke(ContainerInterface $container)
	{
		return new CurrentUserPlugin($container->get('doctrine.entitymanager.orm_default'), $container->get(\Zend\Authentication\AuthenticationService::class));
	}
}



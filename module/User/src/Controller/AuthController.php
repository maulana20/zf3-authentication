<?php
namespace User\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Authentication\Result;
use Zend\Uri\Uri;
use User\Form\LoginForm;
use User\Entity\User;

class AuthController extends AbstractActionController
{
	private $entitymanager;
	private $authmanager;
	private $usermanager;
	
	public function __construct($entitymanager, $authmanager, $usermanager)
	{
		$this->entitymanager = $entitymanager;
		$this->authmanager = $authmanager;
		$this->usermanager = $usermanager;
	}
	
	public function loginAction()
	{
		$redirect_url = (string) $this->params()->fromQuery('redirectUrl', '');
		
		if (strlen($redirect_url) > 2048) throw new \Exception("Too long redirectUrl argument passed");
		
		$this->usermanager->createAdminUserIfNotExists(); // Check if we do not have users in database at all. If so, create
		
		$form = new LoginForm();
		$form->get('redirect_url')->setValue($redirect_url);
		
		$is_error = false;
		if ($this->getRequest()->isPost()) {
			$data = $this->params()->fromPost();
			
			$form->setData($data);
			
			if($form->isValid()) {
				$data = $form->getData();
				$result = $this->authmanager->login($data['email'], $data['password'], $data['remember_me']);
				
				if ($result->getCode() == Result::SUCCESS) {
					$redirect_url = $this->params()->fromPost('redirect_url', '');
					$uri = new Uri($redirect_url);
					
					if (empty($redirect_url)) return $this->redirect()->toRoute('home');
					if ( (!$uri->isValid()) || ($uri->getHost() != null) ) throw new \Exception('Incorrect redirect URL: ' . $redirectUrl);
					
					$this->redirect()->toUrl($redirect_url);
				} else {
					$is_error = true;
				}
			} else {
				$is_error = true;
			}
		}
		
		return new ViewModel([ 'form' => $form, 'isLoginError' => $is_error, 'redirectUrl' => $redirect_url ]);
	}
	
	public function logoutAction()
	{
		$this->authmanager->logout();
		
		return $this->redirect()->toRoute('login');
	}
}

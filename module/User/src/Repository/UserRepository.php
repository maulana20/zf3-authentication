<?php
namespace User\Repository;

use Doctrine\ORM\EntityRepository;
use User\Entity\User;

class UserRepository extends EntityRepository
{
	public function findAllUsers()
	{
		$entitymanager = $this->getEntityManager();
		
		$querybuilder = $entitymanager->createQueryBuilder();
		
		$querybuilder->select('u')->from(User::class, 'u')->orderBy('u.dateCreated', 'DESC');
		
		return $querybuilder->getQuery();
	}
}

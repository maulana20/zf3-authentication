<?php
namespace User\Form;

use Zend\Form\Form;
use Zend\Form\Fieldset;
use Zend\InputFilter\InputFilter;
use User\Validator\UserExistsValidator;

class UserForm extends Form
{
	private $scenario;
	private $entityManager = null;
	private $user = null;
	
	public function __construct($scenario = 'create', $entityManager = null, $user = null)
	{
		parent::__construct('user-form');
		
		$this->setAttribute('method', 'post');
		
		$this->scenario = $scenario;
		$this->entityManager = $entityManager;
		$this->user = $user;
		
		$this->addElements();
		$this->addInputFilter();          
	}
	
	protected function addElements() 
	{
		$this->add([ 'type' => 'text', 'name' => 'email', 'options' => [ 'label' => 'E-mail', ], ]);
		$this->add([ 'type' => 'text', 'name' => 'full_name', 'options' => [ 'label' => 'Full Name', ], ]);
		
		if ($this->scenario == 'create') {
			$this->add([ 'type' => 'password', 'name' => 'password', 'options' => [ 'label' => 'Password', ], ]);
			$this->add([ 'type' => 'password', 'name' => 'confirm_password', 'options' => [ 'label' => 'Confirm password', ], ]);
		}
		
		$this->add([ 'type' => 'select', 'name' => 'status', 'options' => [ 'label' => 'Status', 'value_options' => [ 1 => 'Active', 2 => 'Retired', ] ], ]);
		$this->add([ 'type' => 'submit', 'name' => 'submit', 'attributes' => [ 'value' => 'Create' ], ]);
	}
	
	private function addInputFilter() 
	{
		$inputfilter = $this->getInputFilter();
		
		$inputfilter->add([
			'name' => 'email',
			'required' => true,
			'filters'  => [
				['name' => 'StringTrim'],
			],
			'validators' => [
				[ 'name' => 'StringLength', 'options' => [ 'min' => 1, 'max' => 128 ], ],
				[ 'name' => 'EmailAddress', 'options' => [ 'allow' => \Zend\Validator\Hostname::ALLOW_DNS, 'useMxCheck' => false, ], ],
				[ 'name' => UserExistsValidator::class, 'options' => [ 'entityManager' => $this->entityManager, 'user' => $this->user ], ],                    
			],
		]);
		
		$inputfilter->add([
			'name' => 'full_name',
			'required' => true,
			'filters'  => [
				['name' => 'StringTrim'],
			],
			'validators' => [
				[ 'name' => 'StringLength', 'options' => [ 'min' => 1, 'max' => 512 ], ],
			],
		]);
		
		if ($this->scenario == 'create') {
			$inputfilter->add([
				'name' => 'password',
				'required' => true,
				'filters'  => [],
				'validators' => [
					[ 'name' => 'StringLength', 'options' => [ 'min' => 6, 'max' => 64 ], ],
				],
			]);
			
			$inputfilter->add([
				'name' => 'confirm_password',
				'required' => true,
				'filters' => [],
				'validators' => [
					[ 'name' => 'Identical', 'options' => [ 'token' => 'password', ], ],
				],
			]);
		}
		
		$inputfilter->add([
			'name' => 'status',
			'required' => true,
			'filters'  => [
				['name' => 'ToInt'],
			],
			'validators' => [
				['name' => 'InArray', 'options' => ['haystack' => [1, 2] ] ]
			],
		]);
	}
}

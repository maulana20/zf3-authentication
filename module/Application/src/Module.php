<?php

namespace Application;

use Zend\Mvc\MvcEvent;
use Zend\Session\SessionManager;

class Module
{
	const VERSION = '3.0.0dev';
	
	public function getConfig()
	{
		return include __DIR__ . '/../config/module.config.php';
	}
	
	public function onBootstrap(MvcEvent $event)
	{
		$application = $event->getApplication();
		$servicemanager = $application->getServiceManager();
		$sessionmanager = $servicemanager->get(SessionManager::class);
	}
}

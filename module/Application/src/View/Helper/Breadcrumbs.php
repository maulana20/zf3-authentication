<?php
namespace Application\View\Helper;

use Zend\View\Helper\AbstractHelper;

class Breadcrumbs extends AbstractHelper 
{
	private $items = [];
	
	public function __construct($items=[]) 
	{
		$this->items = $items;
	}
	
	public function setItems($items) 
	{
		$this->items = $items;
	}
	
	public function render() 
	{
		if (count($this->items) == 0) return '';
		
		$result = '<ol class="breadcrumb">';
		
		$number = 1;
		
		foreach ($this->items as $label => $link) {
			$result .= $this->renderItem($label, $link, ($number == count($this->items) ? true : false));
			
			$number++;
		}
		
		$result .= '</ol>';
		
		return $result;
		
	}
	protected function renderItem($label, $link, $is_active) 
	{
		$escape_html = $this->getView()->plugin('escapeHtml');
		
		$result = null;
		
		$result .= $is_active ? '<li class="active">' : '<li>';
		$result .= !$is_active ? '<a href="' . $escape_html($link) . '">' . $escape_html($label) . '</a>' : $escape_html($label);
		$result .= '</li>';
		
		return $result;
	}
}

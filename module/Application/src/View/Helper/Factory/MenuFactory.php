<?php
namespace Application\View\Helper\Factory;

use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;
use Application\View\Helper\Menu;
use Application\Service\NavManager;

class MenuFactory implements FactoryInterface
{
	public function __invoke(ContainerInterface $container, $requested_name, array $options = null)
	{
		$navmanager = $container->get(NavManager::class);
		$items = $navmanager->getMenuItems();
		
		return new Menu($items);
	}
}


<?php
namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use User\Entity\User;

class IndexController extends AbstractActionController 
{
	private $entitymanager;
	
	public function __construct($entitymanager) 
	{
		$this->entitymanager = $entitymanager;
	}
	
	public function indexAction() 
	{
		return new ViewModel();
	}
	
	public function aboutAction() 
	{
		return new ViewModel([ 'appName' => 'User Demo', 'appDescription' => 'This demo shows how to implement user management with Zend Framework 3' ]);
	}
	
	public function settingsAction()
	{
		$user = $this->currentUser();
		
		if ($user == null) throw new \Exception('Not logged in');
		
		return new ViewModel([ 'user' => $user ]);
	}
}

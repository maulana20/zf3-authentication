<?php
namespace Application\Service;

class NavManager
{
	private $auth_service;
	private $url_helper;
	
	public function __construct($auth_service, $url_helper) 
	{
		$this->auth_service = $auth_service;
		$this->url_helper = $url_helper;
	}
	
	public function getMenuItems() 
	{
		$url = $this->url_helper;
		
		$items = [];
		
		$items[] = [ 'id' => 'home', 'label' => 'Home', 'link'  => $url('home') ];
		$items[] = [ 'id' => 'about', 'label' => 'About', 'link'  => $url('about') ];
		
		if (!$this->auth_service->hasIdentity()) {
			$items[] = [ 'id' => 'login', 'label' => 'Sign in', 'link'  => $url('login'), 'float' => 'right' ];
		} else {
			$items[] = [ 'id' => 'admin', 'label' => 'Admin', 'dropdown' => [ [ 'id' => 'users', 'label' => 'Manage Users', 'link' => $url('users') ] ] ];
			$items[] = [ 'id' => 'logout', 'label' => $this->auth_service->getIdentity(), 'float' => 'right', 'dropdown' => [ [ 'id' => 'settings', 'label' => 'Settings', 'link' => $url('application', ['action'=>'settings']) ], [ 'id' => 'logout', 'label' => 'Sign out', 'link' => $url('logout') ], ] ];
		}
		
		return $items;
	}
}


